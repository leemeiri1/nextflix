﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Netflix.Models;

namespace Netflix.Data
{
    public class NetflixContext : DbContext
    {
        public NetflixContext (DbContextOptions<NetflixContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().ToTable(nameof(Movie))
                .HasMany<Genere>(c => c.Generes).WithMany(c => c.Movies);
            base.OnModelCreating(modelBuilder);

        }

        public DbSet<Netflix.Models.Movie> Movie { get; set; }

        public DbSet<Netflix.Models.Review> Review { get; set; }

        public DbSet<Netflix.Models.User> User { get; set; }
       
        public DbSet<Netflix.Models.Comment> Comment { get; set; }
     
        public DbSet<Netflix.Models.Genere> Genere { get; set; }
     
        public DbSet<Netflix.Models.MovieGallery> MovieGallery { get; set; }
    }
}
