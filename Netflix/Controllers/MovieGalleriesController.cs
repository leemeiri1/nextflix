﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Netflix.Data;
using Netflix.Models;

namespace Netflix.Controllers
{
    public class MovieGalleriesController : Controller
    {
        private readonly NetflixContext _context;

        public MovieGalleriesController(NetflixContext context)
        {
            _context = context;
        }

        // GET: MovieGalleries
        public async Task<IActionResult> Index()
        {
            var netflixContext = _context.MovieGallery.Include(m => m.Movie);
            return View(await netflixContext.ToListAsync());
        }

        // GET: MovieGalleries/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movieGallery = await _context.MovieGallery
                .Include(m => m.Movie)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (movieGallery == null)
            {
                return NotFound();
            }

            return View(movieGallery);
        }

        // GET: MovieGalleries/Create
        public IActionResult Create()
        {
            ViewData["MovieID"] = new SelectList(_context.Movie, "Id", "City");
            return View();
        }

        // POST: MovieGalleries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ImageUrl,TrailerUrl,MovieID,Id")] MovieGallery movieGallery)
        {
            if (ModelState.IsValid)
            {
                _context.Add(movieGallery);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieID"] = new SelectList(_context.Movie, "Id", "City", movieGallery.MovieID);
            return View(movieGallery);
        }

        // GET: MovieGalleries/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movieGallery = await _context.MovieGallery.FindAsync(id);
            if (movieGallery == null)
            {
                return NotFound();
            }
            ViewData["MovieID"] = new SelectList(_context.Movie, "Id", "City", movieGallery.MovieID);
            return View(movieGallery);
        }

        // POST: MovieGalleries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ImageUrl,TrailerUrl,MovieID,Id")] MovieGallery movieGallery)
        {
            if (id != movieGallery.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(movieGallery);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MovieGalleryExists(movieGallery.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieID"] = new SelectList(_context.Movie, "Id", "City", movieGallery.MovieID);
            return View(movieGallery);
        }

        // GET: MovieGalleries/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var movieGallery = await _context.MovieGallery
                .Include(m => m.Movie)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (movieGallery == null)
            {
                return NotFound();
            }

            return View(movieGallery);
        }

        // POST: MovieGalleries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var movieGallery = await _context.MovieGallery.FindAsync(id);
            _context.MovieGallery.Remove(movieGallery);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MovieGalleryExists(int id)
        {
            return _context.MovieGallery.Any(e => e.Id == id);
        }
    }
}
