﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Netflix.Data;
using Netflix.Models;

namespace Netflix.Controllers
{
    [Authorize]
    public class GeneresController : Controller
    {
        private readonly NetflixContext _context;

        public GeneresController(NetflixContext context)
        {
            _context = context;
        }

        // GET: Generes
        public async Task<IActionResult> Index()
        {
            return View(await _context.Genere.ToListAsync());
        }

        // GET: Generes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var genere = await _context.Genere
                .FirstOrDefaultAsync(m => m.Id == id);
            if (genere == null)
            {
                return NotFound();
            }

            return View(genere);
        }

        // GET: Generes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Generes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Id")] Genere genere)
        {
            if (ModelState.IsValid)
            {
                if (_context.Genere.Any(x => x.Name == genere.Name))
                {
                    ViewBag.Message = "Already exist";
                }
                else
                {
                    _context.Add(genere);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }

            return View(genere);
        }

        // GET: Generes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var genere = await _context.Genere.FindAsync(id);
            if (genere == null)
            {
                return NotFound();
            }

            return View(genere);
        }

        // POST: Generes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name,Id")] Genere genere)
        {
            if (id != genere.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (_context.Genere.Any(x => x.Name == genere.Name))
                {
                    ViewBag.Message = "Already exist";
                }
                else
                {
                    try
                    {
                        _context.Update(genere);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!GenereExists(genere.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            return View(genere);
        }

        // GET: Generes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var genere = await _context.Genere
                .FirstOrDefaultAsync(m => m.Id == id);
            if (genere == null)
            {
                return NotFound();
            }

            return View(genere);
        }

        // POST: Generes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var genere = await _context.Genere.FindAsync(id);
            _context.Genere.Remove(genere);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GenereExists(int id)
        {
            return _context.Genere.Any(e => e.Id == id);
        }

        [HttpGet]
        public ActionResult Search(string name)
        {
            return View(_context.Genere
                .Where(genere =>
                    (!string.IsNullOrEmpty(name) && genere.Name.ToLower().Contains(name.ToLower()))).ToList());
        }
    }
}
