﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Netflix.Data;
using Netflix.Models;

namespace Netflix.Controllers
{
    [Authorize]
    public class MoviesController : Controller
    {
        private readonly NetflixContext _context;

        public MoviesController(NetflixContext context)
        {
            _context = context;
        }

        // GET: Movies
        public async Task<IActionResult> Index()
        {
            var netflixContext = _context.Movie.Include(m => m.Generes).Include(mg => mg.MovieGallery);
            return View(await netflixContext.ToListAsync());
        }

        // GET: Movies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Movie movie = await _context.Movie
                .Include(m => m.Generes)
                .Include(mg => mg.MovieGallery)
                .FirstOrDefaultAsync(m => m.Id == id);
            
            if (movie == null)
            {
                return NotFound();
            }

            var generesDisp = "";
        foreach (Genere g in movie.Generes)
    {
            generesDisp += ('\n' + g.Name);
    }

            ViewData["GeneresDisplay"] = generesDisp;
            
            return View(movie);
        }

        


        [Authorize(Roles = "Admin")]
        public ActionResult Stats()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetGroupByLocations()
        {

            var LocationToMovies = _context.Movie.GroupBy(m => m.City, m => m.City, (location, movies) => new
            {
                Name = location.ToString(),
                Count = movies.Count()

            }).ToList();

            return Json(LocationToMovies);
        }

        // GET: Movies/Create
        public IActionResult Create()
        {
            ViewData["GeneresList"] = new MultiSelectList(_context.Set<Genere>(), "Id", "Name");
            var values = from City g in Enum.GetValues(typeof(City))
                         select new { Id = g, Name = g.ToString() };
            ViewData["City"] = new SelectList(values, "Id", "Name");
            return View();
        }

        // POST: Movies/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Movie movie)
        {
            if (ModelState.IsValid)
            {
                if (_context.Movie.Any(x => x.Name == movie.Name))
                {
                    ViewData["Message"] = "Already exist";
                }
                else
                {
                    var value = TempData["temp"] as string;
                    string[] generes = value.Split(' ');

                    foreach (var genere in generes)
                    {
                        var gen = from g in _context.Genere
                                         where g.Name == genere
                                         select g;

                        if (gen.Count() > 0)
                        {
                            movie.Generes.Add(gen.First());
                        }
                    }
                    movie.MovieGallery.TrailerUrl = movie.MovieGallery.TrailerUrl.Replace("watch?v=", "embed/");
                    _context.Movie.Add(movie);
                    _context.SaveChanges();
                    return RedirectToAction("Index");
                }

            }
            ViewData["GeneresList"] = new MultiSelectList(_context.Set<Genere>(), "Id", "Name", movie.Generes);

            var values = from City g in Enum.GetValues(typeof(City))
                         select new { Id = g, Name = g.ToString() };
            ViewData["City"] = new SelectList(values, "Id", "Name");
            return View(movie);
        }

        // GET: Movies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            Movie movie = await _context.Movie.Include(mg => mg.MovieGallery).FirstOrDefaultAsync(m => m.Id == id);
            _context.Entry(movie).Collection(p => p.Generes).Load();
            if (movie == null)
            {
                return NotFound();
            }
            ViewData["GeneresList"] = new MultiSelectList(_context.Set<Genere>(), "Id", "Name", movie.Generes);

            String GeneresSel = "";
            foreach (var genere in movie.Generes)
            {
                GeneresSel += (',' + genere.Id.ToString());
            }

            ViewData["GeneresSel"] = GeneresSel;

            var values = from City g in Enum.GetValues(typeof(City))
                         select new { Id = g, Name = g.ToString() };
            ViewData["City"] = new SelectList(values, "Id", "Name");
            return View(movie);
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name,Generes,MovieGallery,City,Id")] Movie movie)
        {
            if (id != movie.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var value = TempData["temp"] as string;

                    if (value != null) {
                     string[] generes = value.Split(',');
                    foreach (var genere in generes)
                    {
                        var gen = from g in _context.Genere
                                  where g.Name == genere
                                  select g;

                        if (gen.Count() > 0)
                        {
                            movie.Generes.Add(gen.First());
                        }
                    }
                    }

                    Movie m = _context.Movie.Include(mg => mg.MovieGallery).Include<Movie, ICollection<Genere>>(u => u.Generes)
                    .FirstOrDefault(m => m.Id == movie.Id);

                    if (m != null)
                    {
                        m.Generes = movie.Generes;
                        m.MovieGallery.ImageUrl = movie.MovieGallery.ImageUrl;
                        m.Name = movie.Name;
                        m.MovieGallery.TrailerUrl = movie.MovieGallery.TrailerUrl;
                        m.City = movie.City;
                    }

                    _context.Entry(m).State = EntityState.Modified;
                    _context.SaveChanges();
                    return RedirectToAction("Index");

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MovieExists(movie.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                //return RedirectToAction(nameof(Index));
            }
            ViewData["GeneresList"] = new MultiSelectList(_context.Set<Genere>(), "Id", "Name", movie.Generes);
            var values = from City g in Enum.GetValues(typeof(City))
                         select new { Id = g, Name = g.ToString() };
            ViewData["City"] = new SelectList(values, "Id", "Name");
            return View(movie);
        }

        // GET: Movies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Movie movie = await _context.Movie
                .Include(m => m.Generes)
                .Include(mg => mg.MovieGallery)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (movie == null)
            {
                return RedirectToAction("Index");
            }

            var generesDisp = "";
        foreach (Genere g in movie.Generes)
    {
            generesDisp += ('\n' + g.Name);
    }

            ViewData["GeneresDisplay"] = generesDisp;
            
            return View(movie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            Movie movie = await _context.Movie.FindAsync(id);
            _context.Movie.Remove(movie);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpGet]
        public ActionResult SearchName(string name)
        {
            return View(_context.Movie
                .Where(movie =>
                    (!string.IsNullOrEmpty(name) && movie.Name.ToLower().Contains(name.ToLower()))).Include(m => m.Generes).Include(mg => mg.MovieGallery).ToList());
        }

        public bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.Id == id);
        }

        //public ICollection<Genere> GetSelectedVals(Movie id)
        //{
        //    Movie m = _context.Movie.Include<Movie, ICollection<Genere>>(u => u.Generes)
        //    .FirstOrDefault(m => m.Id == id.Id);

        //    if(m == null)
        //    {
        //        return null;
        //    }

        //    return m.Generes;
        //}
        

        [HttpPost]
        public bool SetSessionGeneres(Genere labels) {
            // Set to Session here.
            TempData["temp"] = labels.Name;
            return true;
        }
    } 
}
