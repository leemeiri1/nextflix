﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Netflix.Data;
using Netflix.Models;

namespace Netflix.Controllers
{
    [Authorize]
    public class ReviewsController : Controller
    {
        private readonly NetflixContext _context;

        public ReviewsController(NetflixContext context)
        {
            _context = context;
        }

        // GET: Reviews
        public async Task<IActionResult> Index()
        {
            var netflixContext = _context.Review.Include(r => r.Movie).Include(r => r.User).Include(r => r.Comments);
            return View(await netflixContext.ToListAsync());
        }

        // GET: Reviews/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var review = await _context.Review
                .Include(r => r.Movie)
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (review == null)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(review);
        }

        // GET: Reviews/Create
        public IActionResult Create()
        {
            ViewData["MovieID"] = new SelectList(_context.Movie, "Id", "Name");
            ViewData["UserID"] = new SelectList(_context.Set<User>(), "Id", "Username");
            return View();
        }

        public async Task<IActionResult> PostComment(int userId, int reviewId, string content)
        {
            _context.Comment.Add(new Comment
            {
                Content = content,
                UserID = userId,
                ReviewID = reviewId,
                CreationDate = DateTime.Now
            });

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // POST: Reviews/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,Content,CreationDate,UserID,indLike,MovieID,Id")] Review review)
        {
            if (ModelState.IsValid)
            {
                review.CreationDate = DateTime.Now;
                _context.Add(review);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieID"] = new SelectList(_context.Movie, "Id", "Name", review.MovieID);
            ViewData["UserID"] = new SelectList(_context.Set<User>(), "Id", "Username", review.UserID);
            return View(review);
        }

        // GET: Reviews/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var review = await _context.Review.FindAsync(id);
            if (review == null)
            {
                return RedirectToAction(nameof(Index)); ;
            }
            ViewData["MovieID"] = new SelectList(_context.Movie, "Id", "Name", review.MovieID);
            ViewData["UserID"] = new SelectList(_context.Set<User>(), "Id", "Username", review.UserID);
            return View(review);
        }

        // POST: Reviews/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Title,Content,CreationDate,UserID,indLike,MovieID,Id")] Review review)
        {
            if (id != review.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    review.CreationDate = DateTime.Now;
                    _context.Update(review);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReviewExists(review.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MovieID"] = new SelectList(_context.Movie, "Id", "Name", review.MovieID);
            ViewData["UserID"] = new SelectList(_context.Set<User>(), "Id", "Username", review.UserID);
            return View(review);
        }

        // GET: Reviews/Delete/5

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var review = await _context.Review
                .Include(r => r.Movie)
                .Include(r => r.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (review == null)
            {
                return RedirectToAction(nameof(Index));
            }

            return View(review);
        }

        // POST: Reviews/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var review = await _context.Review.FindAsync(id);
            _context.Review.Remove(review);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ReviewExists(int id)
        {
            return _context.Review.Any(e => e.Id == id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Search(string content, string title, DateTime? date, string movieName)
        {
            var dayAfterDate = date?.AddDays(1);

            if(string.IsNullOrEmpty(content) && string.IsNullOrEmpty(title) && string.IsNullOrEmpty(movieName) && !date.HasValue)
            {
                return View(_context.Review
               .OrderByDescending(x => x.CreationDate)
               .Include(r => r.User)
               .Include(r => r.Comments)
               .Include(r => r.Movie)
               .ToList());
            }

            return View(_context.Review
                .Where(review =>
                    (!string.IsNullOrEmpty(content) && review.Content.ToLower().Contains(content.ToLower())) ||
                    (!string.IsNullOrEmpty(movieName) && review.Movie.Name.ToLower().Contains(movieName.ToLower())) ||
                    (!string.IsNullOrEmpty(title) && review.Title.ToLower().Contains(title.ToLower())) ||
                    (date.HasValue && dayAfterDate.HasValue && date < review.CreationDate && review.CreationDate < dayAfterDate))
                .OrderByDescending(x => x.CreationDate)
                .Include(r => r.User)
                .Include(r => r.Comments)
                .Include(r => r.Movie)
                .ToList());
        }
    }
}
