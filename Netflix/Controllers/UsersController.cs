﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Netflix.Data;
using Netflix.Models;

namespace Netflix.Controllers
{
    public class UsersController : Controller
    {
        private readonly NetflixContext _context;

        public UsersController(NetflixContext context)
        {
            _context = context;
        }

        // GET: Users

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.User.ToListAsync());
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult AccessDenied()
        {
            return View();
        }

        public ActionResult FailedLogin()
        {
            return View();
        }


        // GET: Users/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create

        public IActionResult Create()
        {
            var values = from Gender g in Enum.GetValues(typeof(Gender))
                         select new { Id = g, Name = g.ToString() };
            ViewData["Gender"] = new SelectList(values, "Id", "Name");
            return View();
        }

        public async Task<IActionResult> Logout()
        {

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToAction("Login");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("Id,Username,Password")] User user)
        {
                var userFromDb = from u in _context.User
                        where u.Username == user.Username && u.Password == user.Password
                        select u;

              

                if (userFromDb.Count() > 0)
                {

                    Signin(userFromDb.First());

                    return RedirectToAction(nameof(Index), "Home");
                }
                else
                {
                    ViewBag.Error = "Username and/or password are incorrect.";

                }
            
            return View(user);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public JsonResult GetGroupByGender()
        {

            var genereToUsers = _context.User.GroupBy(u => u.Gender,u => u,(gender, users) => new
            {
                Name = gender.ToString(),
                Count = users.Count()

            }).ToList();

            return Json(genereToUsers);
        }


        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Gender,Username,FirstName,LastName,Password,Id")] User user)
        {
            if (ModelState.IsValid)
            {
                if (!(_context.User.Any(x => x.Username == user.Username)))
                {
                    _context.Add(user);
                    await _context.SaveChangesAsync();
                    // if there is a user on the session - it is the admin so we return to the index
                    if (HttpContext.User.Claims.FirstOrDefault(x => x.Type == "userName") != null)
                    {
                        return View(nameof(Index), await _context.User.ToListAsync());
                    }
                    else
                    {
                        HttpContext.Session.SetString("username", user.Username);
                        Signin(user);
                        return RedirectToAction(nameof(Index), "Home");
                    }
                }  else
                { 
                    ViewBag.Error = "Username Already exist";       
                }
            }


            var values = from Gender g in Enum.GetValues(typeof(Gender))
                         select new { Id = g, Name = g.ToString() };
            ViewData["Gender"] = new SelectList(values, "Id", "Name");
            return View(user);
        }

        // GET: Users/Edit/5
        [Authorize(Roles = "Admin")]

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            var values = from Gender g in Enum.GetValues(typeof(Gender))
                         select new { Id = g, Name = g.ToString() };
            ViewData["Gender"] = new SelectList(values, "Id", "Name");
            
            return View(user);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Search(string userName, string firstName, string lastName)
        {
            return View(nameof(Index) , await _context.User
                .Where(user =>
                    (!string.IsNullOrEmpty(firstName) && user.FirstName.ToLower().Contains(firstName.ToLower())) ||
                    !string.IsNullOrEmpty(lastName) && user.LastName.ToLower().Contains(lastName.ToLower()) ||
                    (!string.IsNullOrEmpty(userName) && user.Username.ToLower().Contains(userName.ToLower()))).ToListAsync());
        }


       

       

        public ActionResult Stats()
        {
            return View();
        }


        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]

        public async Task<IActionResult> Edit(int id, [Bind("Gender,Username,FirstName,LastName,Password,IsAdmin,Id")] User user)
        {
            if (id != user.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Users/Delete/5
        [Authorize(Roles = "Admin")]

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .FirstOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await _context.User.FindAsync(id);
            _context.User.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

         public IActionResult Register()
        {
            var values = from Gender g in Enum.GetValues(typeof(Gender))
                         select new { Id = g, Name = g.ToString() };
            ViewData["Gender"] = new SelectList(values, "Id" , "Name");
            return View();
        }

        private async void Signin(User account)
        {
            String role = "";
            if (account.IsAdmin)
            {
                role = "Admin";
            }
            else
            {
                role = "Client";
            }

            var claims = new List<Claim>
                 {
                    
                     new Claim("userName", account.Username),
                     new Claim("userId", account.Id.ToString()),
                     //new Claim("role", role ),
                     new Claim(ClaimTypes.Role, role )
                 };

             var claimsIdentity = new ClaimsIdentity(
                 claims, CookieAuthenticationDefaults.AuthenticationScheme);

             var authProperties = new AuthenticationProperties
             {
                 ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10)
             };

             await HttpContext.SignInAsync(
                 CookieAuthenticationDefaults.AuthenticationScheme,
                 new ClaimsPrincipal(claimsIdentity),
                 authProperties);
         
            
        }

        [Authorize(Roles = "Admin")]
        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.Id == id);
        }
    }
}
