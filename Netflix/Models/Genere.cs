﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Netflix.Models
{
    public class Genere : BaseModel
    {
        [Required]
        [MinLength(2)]
        [DisplayName("Genre Name")]
        public string Name { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }

    }
}