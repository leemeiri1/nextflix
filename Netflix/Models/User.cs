﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Netflix.Models
{
    public class User : BaseModel
    {
        public Gender Gender { get; set; }

        [Required]
        [MinLength(2)]
        [DisplayName("Username")]
        public string Username { get; set; }

        [Required]
        [MinLength(2)]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Your first name can only contain letters and white spaces")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2)]
        [RegularExpression("^[a-zA-Z ]*$", ErrorMessage = "Your last name can only contain letters and white spaces")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [Required]
        [MinLength(8)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DisplayName("Administrator")]
        public bool IsAdmin { get; set; }

        public virtual List<Comment> Comments { get; set; }
        public virtual List<Review> Reviews { get; set; }

    }
}