﻿namespace Netflix.Models
{
    public enum Gender
    {
        Male,
        Female,
        Other
    }
}