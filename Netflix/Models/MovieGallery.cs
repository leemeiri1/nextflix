﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Netflix.Models
{
    public class MovieGallery : BaseModel
    {
        [Required]
        [DisplayName("Image")]
        public string ImageUrl { get; set; }

        [Required]
        [DisplayName("Trailer")]
        public string TrailerUrl { get; set; }


        [Required]
        [DisplayName("Movie")]
        [ForeignKey("Movie")]
        public int MovieID { get; set; }
        public virtual Movie Movie { get; set; }
    }

}

