﻿namespace Netflix.Models
{
    public enum City
    {
        Ashdod,
        Eilat,
        Haifa,
        Holon,
        Jerusalem,
        Rehovot
    }
}