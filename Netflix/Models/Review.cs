﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Netflix.Models
{
    public class Review : BaseModel
    {
        [MaxLength(20)]
        [MinLength(2)]
        [Required]
        [DisplayName("Title")]
        public string Title { get; set; }

        [MaxLength(8000)]
        [MinLength(2)]
        [Required]
        public string Content { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Created at")]
        public DateTime CreationDate { get; set; }
      
        [Required]
        [DisplayName("User")]
        [ForeignKey("User")]
        public int UserID { get; set; }
        public virtual User User { get; set; }

        [DefaultValue(false)]
        [DisplayName("Like")]
        public bool indLike { get; set; }

        [Required]
        [DisplayName("Movie")]
        [ForeignKey("Movie")]
        public int MovieID { get; set; }
        public virtual Movie Movie { get; set; }

        public virtual List<Comment> Comments { get; set; }
    }
}