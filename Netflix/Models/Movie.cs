﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Netflix.Models
{
    public class Movie : BaseModel
    {
        [Required]
        [DisplayName("Name")]
        public string Name { get; set; }


        [Required]
        [DisplayName("MovieGallery")]
        public int MovieGalleryID { get; set; }
        public virtual MovieGallery MovieGallery { get; set; }

        [Required]
        [DisplayName("City")]
        public string City { get; set; }

        [Required]
        [DisplayName("Genere")]
        public virtual ICollection<Genere> Generes { get; set; }

        public virtual List<Review> Reviews { get; set; }
    }

}

